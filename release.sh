#!/bin/bash

###############################################################################
#                   Release script for neo4j-elasticsearch                    #
#                                                                             #
# Description: create a release from an existing milestone.                   #
# The title of the milestone should follow the format v$MAJOR.$MINOR_$PATCH   #
# Deps:  jq, git, curl                                                        #
#                                                                             #
###############################################################################

# GLOBAL VARIABLES
# GITLAB_ACCESS_TOKEN="" (should be in your env)
GITLAB_PROJECT_ID="ouestware%2Fneo4j-elasticsearch"

# COMPUTED VARIABLES
PROJECT_PATH=$(
  cd $(dirname $0) >/dev/null 2>&1
  pwd -P
)
WORK_DIR="$(mktemp -d)"

#
# Check the status passed as params, and exit if needed.
#
check() {
  local STATUS=$1
  local MSG=$2
  if [ "$STATUS" -ne "0" ]; then
    if [ ! -z "$MSG" ]; then
      echo $"$MSG"
    fi
    exit 1
  fi
}

#
# Check if the git repository is clean (without local changes).
# If so, return `1`, else `0`
#
git_check_local_changes() {
  git update-index -q --refresh
  local CHANGED=$(git diff-index --name-only HEAD --)
  if [ ! -z "$CHANGED" ]; then
    return 1
  else
    return 0
  fi
}

#
# Retrieve all the opened milestone from the gitlab project
#
gitlab_milestones() {
  local RESULT=$(curl -s --header "PRIVATE-TOKEN: $GITLAB_ACCESS_TOKEN" "https://gitlab.com/api/v4/projects/$GITLAB_PROJECT_ID/milestones?state=active" | jq --raw-output '. | map("[\"\(.title)\"]=\"\(.id)\"") | reduce .[] as $item ("milestonesArray=("; . + $item + " ") + ")"')
  check $? "Error when retrieving milestones"
  echo $RESULT
}

gitlab_milestone_issues() {
  local MILESTONE_ID=$1
  curl -s --header "PRIVATE-TOKEN: $GITLAB_ACCESS_TOKEN" "https://gitlab.com/api/v4/projects/$GITLAB_PROJECT_ID/milestones/$MILESTONE_ID/issues" | jq -r -c '.[] | select(.state | contains("closed")) | [.iid, .title] | @tsv'
}

gitlab_milestone_mergerequest() {
  local MILESTONE_ID=$1
  curl -s --header "PRIVATE-TOKEN: $GITLAB_ACCESS_TOKEN" "https://gitlab.com/api/v4/projects/$GITLAB_PROJECT_ID/milestones/$MILESTONE_ID/merge_requests" | jq -r -c '.[] | select(.state | contains("closed")) | [.iid, .title] | @tsv'
}

gitlab_milestone_changelog() {
  local MILESTONE_ID=$1
  local MILESTONE_TITLE=$(curl -s --header "PRIVATE-TOKEN: $GITLAB_ACCESS_TOKEN" https://gitlab.com/api/v4/projects/$GITLAB_PROJECT_ID/milestones/$MILESTONE_ID | jq -r '.title')
  local ISSUES=$(gitlab_milestone_issues $MILESTONE_ID)
  local MR=$(gitlab_milestone_mergerequest $MILESTONE_ID)

  echo "# Release $MILESTONE_TITLE" >"$WORK_DIR/CHANGELOG.md"
  echo "" >>"$WORK_DIR/CHANGELOG.md"

  if [ ! -z "$ISSUES" ]; then
    echo "## Issues" >>"$WORK_DIR/CHANGELOG.md"
    echo "" >>"$WORK_DIR/CHANGELOG.md"
    echo -e "$ISSUES" | sed s/^/\*\ #/g >>"$WORK_DIR/CHANGELOG.md"
    echo "" >>"$WORK_DIR/CHANGELOG.md"
  fi

  if [ ! -z "$MR" ]; then
    echo "## Merge Requests" >>"$WORK_DIR/CHANGELOG.md"
    echo "" >>"$WORK_DIR/CHANGELOG.md"
    echo -e "$MR" | sed s/^/\*\ #/g >>"$WORK_DIR/CHANGELOG.md"
  fi
}

#
# Create the release on git and gitlab for the specified milestone id.
#   - retieve the milestone title
#   - compute the version from the milestone title
#   - change projects version
#   - commit the changes with the message `Release v1.0.0`
#   - create the tag
#   - Create the changelog
#   - Create the package
#   - push to the origin
#   - create the release on gitlab
#
gitlab_make_release() {
  local MILESTONE_ID=$1

  echo
  echo " - Check for local changes"
  git_check_local_changes
  check $? "You have some local changes"

  # retrieve the release from gitlab
  echo
  echo " - Retrieve milestone/release info from gitlab"
  local MILESTONE_TITLE=$(curl -s --header "PRIVATE-TOKEN: $GITLAB_ACCESS_TOKEN" https://gitlab.com/api/v4/projects/$GITLAB_PROJECT_ID/milestones/$MILESTONE_ID | jq -r '.title')
  if [ -z "$MILESTONE_TITLE" ]; then
    exit 1
  fi
  local MILESTONE_VERSION=${MILESTONE_TITLE:1}

  echo
  echo " - Changing project version"
  project_change_version $MILESTONE_VERSION
  check $? "An error occured when changing project version"

  echo
  echo " - Commit for the release"
  git commit -am "Release $MILESTONE_TITLE"
  #check $? "An error occured when creating release commit"

  # create the tag
  echo
  echo " - Creating the git tag"
  echo "$MILESTONE_VERSION $MILESTONE_TITLE"
  git tag -a $MILESTONE_TITLE -m "Version $MILESTONE_VERSION"
  check $? "An error occured when creating tag release"

  echo
  echo " - Pushing to gitlab"
  git push origin $MILESTONE_TITLE
  check $? "An error occured when pushing on gitlab"
  git push origin
  check $? "An error occured when pushing on gitlab"

  echo
  echo " - Creating the changelog"
  gitlab_milestone_changelog $MILESTONE_ID
  check $? "An error occured during the creation of the changelog"

  echo
  echo " - Making the package"
  project_package $MILESTONE_VERSION
  check $? "An error occured during the creation of the package"

  echo
  echo " - Create the release on gitlab"
  local DESCRIPTION=$(cat $WORK_DIR/CHANGELOG.md)
  local JAR_NAME="neo4j-elasticsearch-$MILESTONE_VERSION-all.jar"
  local JAR_URL="https://oss.sonatype.org/service/local/repositories/releases/content/com/ouestware/neo4j-elasticsearch/$MILESTONE_VERSION/neo4j-elasticsearch-$MILESTONE_VERSION-all.jar"
  jq -n \
    --arg TAG "$MILESTONE_TITLE" \
    --arg DESC "$DESCRIPTION" \
    --arg ML "$MILESTONE_TITLE" \
    --arg LINK_NAME "$JAR_NAME" \
    --arg LINK_URL "$JAR_URL" \
    '{ tag_name: $TAG, description: $DESC, milestones: [$ML], assets: { links: [ {name:$LINK_NAME, url:$LINK_URL} ] } }' |
    curl -v -d@- \
      --header 'Content-Type: application/json' --header "PRIVATE-TOKEN: $GITLAB_ACCESS_TOKEN" \
      --request POST "https://gitlab.com/api/v4/projects/$GITLAB_PROJECT_ID/releases"
  check $? "An error occured when creating release on gitlab"
}

#
# Run all the tests of the project
#
project_test() {
  cd $PROJECT_PATH
  mvn test
  return $?
}

#
# Package all the component of the project
#
project_package() {
  echo "Packaging is done by gitlab ci"
}

#
# Change the project version
#
project_change_version() {
  local VERSION=$1
  cd $PROJECT_PATH
  mvn versions:set -DnewVersion=$VERSION
  mvn versions:commit
  return $?
}

#############################################################################

echo
echo " ~~~ Check local changes ~~~ "
echo
git_check_local_changes
check $? "You have some local changes"

echo
echo " ~~~ Git Pull ~~~ "
echo
git pull
check $? "Git pull failed"

# Display the list of availables milestones
temp=$(gitlab_milestones)
declare -A "${temp}"

echo
echo " ~~~ Select a Milestone ~~~ "
echo
PS3="Your choice> "
select opt in "${!milestonesArray[@]}"; do
  case "$REPLY" in
  *)
    MILESTONE_TITLE=$opt
    break
    ;;
  esac
done
MILESTONE_ID=${milestonesArray[$MILESTONE_TITLE]}
echo "Release for $MILESTONE_TITLE ( $MILESTONE_ID ) will be done"

echo
echo " ~~~ Running all tests ~~~ "
echo
project_test
check $? "You have some tests in failure"

echo
echo " ~~~ Performing the release ~~~ "
echo
gitlab_make_release $MILESTONE_ID
check $? "Release process failed"

echo
echo " /!\ Your release package is located here -> $WORK_DIR"

echo
echo " ~~~ Next version ~~~ "
echo
read -e -p "What is the next version (format: X.Y_Z)? " -i "$NEXT_VERSION" NEXT_VERSION

project_change_version "${NEXT_VERSION}-SNAPSHOT"
check $? "An error occured when changing project version"

git commit -am "Prepare next version"
git push origin

exit 0
