package org.ouestware.neo4j.elasticsearch.test;

import io.searchbox.client.JestClient;
import io.searchbox.client.JestClientFactory;
import io.searchbox.client.config.HttpClientConfig;
import io.searchbox.core.Search;
import io.searchbox.core.SearchResult;
import io.searchbox.indices.CreateIndex;
import io.searchbox.indices.DeleteIndex;
import io.searchbox.indices.mapping.PutMapping;
import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.neo4j.configuration.GraphDatabaseSettings;
import org.neo4j.driver.Config;
import org.neo4j.driver.Driver;
import org.neo4j.driver.GraphDatabase;
import org.neo4j.driver.Session;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Transaction;
import org.neo4j.harness.junit.rule.Neo4jRule;
import org.ouestware.neo4j.elasticsearch.ElasticSearchKernelExtensionFactory;
import org.ouestware.neo4j.elasticsearch.ElasticSearchProcedures;
import org.ouestware.neo4j.elasticsearch.config.ElasticSearchNeo4jConfig;
import org.ouestware.neo4j.elasticsearch.config.ElasticSearchIndexSpec;
import org.ouestware.neo4j.elasticsearch.config.ElasticSearchIndexSpecParser;
import org.testcontainers.elasticsearch.ElasticsearchContainer;

import java.io.IOException;
import java.text.ParseException;
import java.time.Duration;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public abstract class ElasticSearchIntegrationTest {

    @ClassRule
    public final static ElasticsearchContainer container = new ElasticsearchContainer("docker.elastic.co/elasticsearch/elasticsearch:" + System.getenv().getOrDefault("TEST_ES_VERSION", "8.6.0"))
            .withPassword("l3tm31n")
            .withStartupTimeout(Duration.ofSeconds(90))
            .withEnv("xpack.security.enabled", "false");
    @Rule
    public Neo4jRule neo4j;
    public Map<String, List<ElasticSearchIndexSpec>> specs;
    public String dbInitScriptPath = null;

    public ElasticSearchIntegrationTest(String specs) throws ParseException {
        this.specs = ElasticSearchIndexSpecParser.parseIndexSpec(specs);

        ElasticSearchNeo4jConfig esConfig = ElasticSearchNeo4jConfig.forDatabase(GraphDatabaseSettings.DEFAULT_DATABASE_NAME);
        neo4j = new Neo4jRule()
                .withProcedure(ElasticSearchProcedures.class)
                .withUnmanagedExtension("/", ElasticSearchKernelExtensionFactory.class)
                .withConfig(esConfig.CONFIG_ES_URL, "http://" + container.getHttpHostAddress())
                .withConfig(esConfig.CONFIG_ES_INDEX_SPEC, specs)
                .withConfig(esConfig.CONFIG_ES_USE_INDEX_TYPE, Boolean.FALSE)
                .withConfig(esConfig.CONFIG_ES_INCLUDE_DB, Boolean.TRUE)
                .withConfig(esConfig.CONFIG_ES_ASYNC, Boolean.FALSE)
                .withConfig(esConfig.CONFIG_ES_USER, "elastic")
                .withConfig(esConfig.CONFIG_ES_PASSWORD, "l3tm31n");
    }

    public ElasticSearchIntegrationTest(String specs, String dbInitScriptPath) throws ParseException {
        this(specs);
        this.dbInitScriptPath = dbInitScriptPath;
    }


    public void checkNumberOfIndexedDocument(String label, int expected) throws IOException, InterruptedException {
        // Check the index
        String query = "{\n" +
                "  \"query\": {\n" +
                "    \"match\": {\n" +
                "      \"@labels\": \"" + label + "\"\n" +
                "    }\n" +
                "  }\n" +
                "}";

        // Wait for the elasticsearch refresh
        Thread.sleep(2000);

        try (JestClient client = getJestClient()) {
            SearchResult result = client.execute(new Search.Builder(query).build());
            assertTrue(result.getErrorMessage(), result.isSucceeded());
            assertEquals(expected, result.getJsonObject().get("hits").getAsJsonObject().get("total").getAsJsonObject().get("value").getAsInt());
        }
    }

    public JestClient getJestClient() {
        // Create ES JEST client
        JestClientFactory factory = new JestClientFactory();
        factory.setHttpClientConfig(new HttpClientConfig
                .Builder("http://" + container.getHttpHostAddress()).defaultCredentials("elastic", "l3tm31n")
                .readTimeout(5000)
                .connTimeout(5000)
                .build());
        return factory.getObject();
    }

    public void resetIndex() throws Exception {
        try (JestClient client = getJestClient();) {
            for (List<ElasticSearchIndexSpec> specsForLabel : this.specs.values()) {
                for (ElasticSearchIndexSpec spec : specsForLabel) {
                    client.execute(new DeleteIndex.Builder(spec.getIndexName()).build());
                    client.execute(new CreateIndex.Builder(spec.getIndexName()).build());
                }
            }
        }
    }

    public void createMappingFor(String index, String mapping) throws Exception {
        try (JestClient client = getJestClient()) {
            PutMapping putMapping = new PutMapping.Builder(
                    index,
                    null,
                    mapping
            ).build();
            client.execute(putMapping);
        }
    }

    public String getEsNodeId(Node node) {
        return node.getElementId();
    }

    public Driver getNeo4jDriver() {
        return GraphDatabase.driver(neo4j.boltURI(), Config.builder().withoutEncryption().build());
    }

    public Node createNode(String label, Map<String, String> properties) {
        try (Transaction tx = neo4j.defaultDatabaseService().beginTx()) {
            Node node = tx.createNode(Label.label(label));
            for (Map.Entry<String, String> entry : properties.entrySet()) {
                node.setProperty(entry.getKey(), entry.getValue());
            }
            tx.commit();
            return node;
        }
    }

    @Before
    public void setUp() throws Exception {
        // init db if needed
        if (this.dbInitScriptPath != null) {
            String query = "";
            try (Scanner s = new Scanner(ElasticSearchIntegrationTest.class.getResourceAsStream("/cypher/movie.cyp")).useDelimiter("\\n")) {
                while (s.hasNext()) {
                    query += s.next() + "\n";
                }
            }
            Driver driver = getNeo4jDriver();
            try (Session session = driver.session()) {
                session.run(query);
            }
        }

        // Create ES indices
        try (JestClient client = getJestClient();) {
            for (List<ElasticSearchIndexSpec> specsForLabel : this.specs.values()) {
                for (ElasticSearchIndexSpec spec : specsForLabel) {
                    client.execute(new CreateIndex.Builder(spec.getIndexName()).build());
                }
            }
        }
    }

    @After
    public void tearDown() throws Exception {
        // Reset Neo4j db
        Driver driver = getNeo4jDriver();
        try (Session session = driver.session()) {
            session.run("MATCH (n) DETACH DELETE n");
        }
        // Delete ES indices
        try (JestClient client = getJestClient();) {
            for (List<ElasticSearchIndexSpec> specsForLabel : this.specs.values()) {
                for (ElasticSearchIndexSpec spec : specsForLabel) {
                    client.execute(new DeleteIndex.Builder(spec.getIndexName()).build());
                }
            }
        }
        driver.close();
    }

}
