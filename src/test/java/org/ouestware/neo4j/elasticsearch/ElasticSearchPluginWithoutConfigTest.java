package org.ouestware.neo4j.elasticsearch;

import org.junit.Rule;
import org.junit.Test;
import org.neo4j.driver.Config;
import org.neo4j.driver.Driver;
import org.neo4j.driver.GraphDatabase;
import org.neo4j.driver.Session;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Transaction;
import org.neo4j.harness.junit.rule.Neo4jRule;

import static org.junit.Assert.*;

public class ElasticSearchPluginWithoutConfigTest {

    @Rule
    public Neo4jRule neo4j;

    public ElasticSearchPluginWithoutConfigTest() {
        neo4j = new Neo4jRule()
                .withProcedure(ElasticSearchProcedures.class)
                .withUnmanagedExtension("/", ElasticSearchKernelExtensionFactory.class);
        this.neo4j.dumpLogsOnFailure(System.out);
    }

    public Driver getNeo4jDriver() {
        return GraphDatabase.driver(neo4j.boltURI(), Config.builder().withoutEncryption().build());
    }

    @Test
    public void neo4j_should_start_without_any_es_configuration() throws Exception {
        Node node = null;
        try (Transaction tx = neo4j.defaultDatabaseService().beginTx()) {
            node = tx.createNode(Label.label("JustATest"));
            tx.commit();
        }
        assertNotNull(node);
    }

    @Test
    public void calling_procedure_indexall_on_a_db_without_configuration_should_return_a_specific_error() throws Exception {
        // Run the procedure
        Driver driver = getNeo4jDriver();
        try (Session session = driver.session()) {
            session.run("CALL elasticsearch.indexAll({});").consume();
            fail();
        } catch (Exception e) {
            assertEquals(
                    "Failed to invoke procedure `elasticsearch.indexAll`: Caused by: java.lang.Exception: ElasticSearch configuration is not defined for database neo4j",
                    e.getMessage()
            );
        }
    }


}
