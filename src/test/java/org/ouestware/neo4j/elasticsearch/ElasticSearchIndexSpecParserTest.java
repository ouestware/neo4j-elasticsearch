package org.ouestware.neo4j.elasticsearch;

import org.junit.Test;
import org.ouestware.neo4j.elasticsearch.config.ElasticSearchIndexSpec;

import java.text.ParseException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.ouestware.neo4j.elasticsearch.config.ElasticSearchIndexSpecParser.parseIndexSpec;

public class ElasticSearchIndexSpecParserTest {

    @Test
    public void parsing_index_spec_should_work() throws ParseException {
        Map<String, List<ElasticSearchIndexSpec>> rv = parseIndexSpec("index_name:Label(foo,bar,quux),other_index_name:OtherLabel(baz,quuxor)");
        assertEquals(2, rv.size());
        assertEquals(new HashSet<>(asList("Label", "OtherLabel")), rv.keySet());

        // testing first group
        assertEquals("index_name", rv.get("Label").get(0).getIndexName());
        assertEquals(new HashSet<>(asList("foo", "bar", "quux")), rv.get("Label").get(0).getProperties());

        // testing second group
        assertEquals("other_index_name", rv.get("OtherLabel").get(0).getIndexName());
        assertEquals(new HashSet<>(asList("baz", "quuxor")), rv.get("OtherLabel").get(0).getProperties());
    }

    @Test
    public void parsing_index_spec_with_indexname_defined_twice_should_work() throws ParseException {
        Map<String, List<ElasticSearchIndexSpec>> rv = parseIndexSpec("index_name:Label(foo,bar),other_index_name:Label(quux)");
        assertEquals(1, rv.size());
        assertEquals(2, rv.get("Label").size());

        // testing first group
        assertEquals("index_name", rv.get("Label").get(0).getIndexName());
        assertEquals(new HashSet<>(asList("foo", "bar")), rv.get("Label").get(0).getProperties());

        // testing second group
        assertEquals("other_index_name", rv.get("Label").get(1).getIndexName());
        assertEquals(new HashSet<>(asList("quux")), rv.get("Label").get(1).getProperties());
    }

    @Test
    public void parsing_index_spec_with_cypher_should_work() throws ParseException {
        String cypher = "MATCH (this)-[:MY_REL]->(n:OtherLabel) RETURN { id:id(n) }";
        Map<String, List<ElasticSearchIndexSpec>> rv = parseIndexSpec("index_name:Label(cql`" + cypher + "`cql)");
        assertEquals(1, rv.size());
        assertEquals(1, rv.get("Label").size());
        assertEquals(cypher, rv.get("Label").get(0).getCypher());
    }

    @Test
    public void parsing_index_spec_with_a_mixed_should_work() throws ParseException {
        String cypher = "MATCH (this)-[:MY_REL]->(n:`OtherLabel`) RETURN { id:id(n) }";
        Map<String, List<ElasticSearchIndexSpec>> rv = parseIndexSpec("index_name:Label(cql`" + cypher + "`cql),index_name_2:Label2(foo,bar),other_index_name:OtherLabel(quux),other_index_name_2:OtherLabel(cql`" + cypher + "`cql)");
        assertEquals(3, rv.size());

        // testing first group
        assertEquals(1, rv.get("Label").size());
        assertEquals("index_name", rv.get("Label").get(0).getIndexName());
        assertEquals(cypher, rv.get("Label").get(0).getCypher());

        // testing second group
        assertEquals(1, rv.get("Label2").size());
        assertEquals("index_name_2", rv.get("Label2").get(0).getIndexName());
        assertEquals(new HashSet<>(asList("foo", "bar")), rv.get("Label2").get(0).getProperties());

        // testing third group
        assertEquals(2, rv.get("OtherLabel").size());
        assertEquals("other_index_name", rv.get("OtherLabel").get(0).getIndexName());
        assertEquals(new HashSet<>(asList("quux")), rv.get("OtherLabel").get(0).getProperties());

        // testing forth group
        assertEquals("other_index_name_2", rv.get("OtherLabel").get(1).getIndexName());
        assertEquals(cypher, rv.get("OtherLabel").get(1).getCypher());
    }

    @Test(expected = ParseException.class)
    public void parsing_bad_index_spec_should_return_an_exception() throws ParseException {
        Map rv = parseIndexSpec("index_name:Label(foo,bar");
    }

    @Test(expected = ParseException.class)
    public void parsing_index_spec_without_props_should_return_an_exception() throws ParseException {
        Map rv = parseIndexSpec("index_name:Label()");
    }

    @Test(expected = ParseException.class)
    public void parsing_index_spec_without_label_should_return_an_exception() throws ParseException {
        Map rv = parseIndexSpec("index_name");
    }

}
