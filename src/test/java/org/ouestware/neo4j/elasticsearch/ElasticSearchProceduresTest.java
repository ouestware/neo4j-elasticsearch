package org.ouestware.neo4j.elasticsearch;

import org.junit.Test;
import org.neo4j.driver.Driver;
import org.neo4j.driver.Record;
import org.neo4j.driver.Result;
import org.neo4j.driver.Session;
import org.ouestware.neo4j.elasticsearch.test.ElasticSearchIntegrationTest;

import java.text.ParseException;

import static org.junit.Assert.assertEquals;

public class ElasticSearchProceduresTest extends ElasticSearchIntegrationTest {

    private final static String label = "MyLabel";
    private final static String index = "index_name";
    private final static String[] properties = new String[]{"foo", "hello"};

    public ElasticSearchProceduresTest() throws ParseException {
        super(index + ":" + label + "(" + String.join(", ", properties) + ")");
    }


    @Test
    public void procedure_indexall_should_work() throws Exception {
        Driver driver = getNeo4jDriver();

        // Load some data into Neo4j
        try (Session session = driver.session()) {
            session.run("UNWIND range(1, 101, 1) AS index CREATE (:MyLabel { foo: 'foo_' + index}) RETURN count(*) AS count");
        }

        // Reset the index
        resetIndex();

        // Run the procedure
        try (Session session = driver.session()) {
            Result rs = session.run("CALL elasticsearch.indexAll({ batchSize:50, async:false });");

            // Check the return of the procedure
            Record record = rs.single();
            assertEquals(3, record.get("numberOfBatches").asInt());
            assertEquals(101, record.get("numberOfIndexedDocument").asInt());
        }

        this.checkNumberOfIndexedDocument("MyLabel", 101);

    }

    @Test
    public void calling_procedure_indexall_with_empty_config_should_work() throws Exception {
        Driver driver = getNeo4jDriver();
        // Run the procedure
        try (Session session = driver.session()) {
            session.run("CALL elasticsearch.indexAll({});");
        }
    }

    @Test
    public void calling_procedure_indexNodes_should_work() throws Exception {
        Driver driver = getNeo4jDriver();

        // Load some data into Neo4j
        try (Session session = driver.session()) {
            session.run("UNWIND range(1, 101, 1) AS index CREATE (:MyLabel { foo: 'foo_' + index}) RETURN count(*) AS count");
        }

        // Reset the index
        resetIndex();

        // Run the procedure
        try (Session session = driver.session()) {
            Result rs = session.run("MATCH (n:MyLabel) WITH collect(n)[0..50] AS nodes CALL elasticsearch.indexNodes(nodes, { async:false }) YIELD numberOfBatches, numberOfIndexedDocument RETURN numberOfBatches, numberOfIndexedDocument;");

            // Check the return of the procedure
            Record record = rs.single();
            assertEquals(1, record.get("numberOfBatches").asInt());
            assertEquals(50, record.get("numberOfIndexedDocument").asInt());
        }

        this.checkNumberOfIndexedDocument("MyLabel", 50);
    }

    @Test
    public void calling_procedure_indexNodes_with_empty_params_should_work() throws Exception {
        Driver driver = getNeo4jDriver();
        // Run the procedure
        try (Session session = driver.session()) {
            session.run("CALL elasticsearch.indexNodes([], {});");
        }
    }

    @Test
    public void calling_procedure_indexNodes_on_unknown_nodes_should_work() throws Exception {
        Driver driver = getNeo4jDriver();

        // Load some data into Neo4j
        try (Session session = driver.session()) {
            session.run("UNWIND range(1, 100, 1) AS index CREATE (:UnknownLabel { foo: 'foo_' + index}) RETURN count(*) AS count");
        }

        // Reset the index
        resetIndex();

        // Run the procedure
        try (Session session = driver.session()) {
            Result rs = session.run("MATCH (n:UnknownLabel) WITH collect(n)[0..50] AS nodes CALL elasticsearch.indexNodes(nodes, { async:false }) YIELD numberOfBatches, numberOfIndexedDocument RETURN numberOfBatches, numberOfIndexedDocument;");

            // Check the return of the procedure
            Record record = rs.single();
            assertEquals(1, record.get("numberOfBatches").asInt());
            assertEquals(0, record.get("numberOfIndexedDocument").asInt());
        }
    }

    @Test
    public void calling_procedure_deleteNodes_should_work() throws Exception {
        Driver driver = getNeo4jDriver();

        // Load some data into Neo4j
        try (Session session = driver.session()) {
            session.run("UNWIND range(1, 100, 1) AS index CREATE (:MyLabel { foo: 'foo_' + index}) RETURN count(*) AS count");
        }

        // Run the procedure
        try (Session session = driver.session()) {
            Result rs = session.run("MATCH (n:MyLabel) WITH collect(n)[0..50] AS nodes CALL elasticsearch.deleteNodes(nodes, { async:false }) YIELD numberOfBatches, numberOfIndexedDocument RETURN numberOfBatches, numberOfIndexedDocument;");

            // Check the return of the procedure
            Record record = rs.single();
            assertEquals(1, record.get("numberOfBatches").asInt());
            assertEquals(50, record.get("numberOfIndexedDocument").asInt());
        }

        this.checkNumberOfIndexedDocument("MyLabel", 50);
    }

    @Test
    public void calling_procedure_deleteNodesByElementId_should_work() throws Exception {
        Driver driver = getNeo4jDriver();

        // Load some data into Neo4j
        try (Session session = driver.session()) {
            session.run("UNWIND range(1, 100, 1) AS index CREATE (:MyLabel { foo: 'foo_' + index}) RETURN count(*) AS count");
        }

        this.checkNumberOfIndexedDocument("MyLabel", 100);

        // Run the procedure
        try (Session session = driver.session()) {
            Result rs = session.run("MATCH (n:MyLabel) WITH collect(elementId(n))[0..50] AS nodes CALL elasticsearch.deleteNodesByElementId(nodes, { async:false }) YIELD numberOfBatches, numberOfIndexedDocument RETURN numberOfBatches, numberOfIndexedDocument;");

            // Check the return of the procedure
            Record record = rs.single();
            assertEquals(1, record.get("numberOfBatches").asInt());
            assertEquals(50, record.get("numberOfIndexedDocument").asInt());
        }

        this.checkNumberOfIndexedDocument("MyLabel", 50);
    }

    @Test
    public void calling_procedure_deleteNodesByElementId_with_bad_index_should_work() throws Exception {
        Driver driver = getNeo4jDriver();

        // Load some data into Neo4j
        try (Session session = driver.session()) {
            session.run("UNWIND range(1, 100, 1) AS index CREATE (:MyLabel { foo: 'foo_' + index}) RETURN count(*) AS count");
        }

        // Run the procedure
        try (Session session = driver.session()) {
            Result rs = session.run("MATCH (n:MyLabel) WITH collect(elementId(n))[0..50] AS nodes CALL elasticsearch.deleteNodesByElementId(nodes, { async:false, indices:['bla'] }) YIELD numberOfBatches, numberOfIndexedDocument RETURN numberOfBatches, numberOfIndexedDocument;");

            // Check the return of the procedure
            Record record = rs.single();
            assertEquals(1, record.get("numberOfBatches").asInt());
            assertEquals(0, record.get("numberOfIndexedDocument").asInt());
        }

        this.checkNumberOfIndexedDocument("MyLabel", 100);
    }


}
