package org.ouestware.neo4j.elasticsearch;

import com.google.gson.JsonObject;
import io.searchbox.client.JestClient;
import io.searchbox.client.JestResult;
import io.searchbox.core.Get;
import io.searchbox.core.Search;
import io.searchbox.core.SearchResult;
import org.junit.Test;
import org.neo4j.configuration.GraphDatabaseSettings;
import org.neo4j.driver.Driver;
import org.neo4j.driver.Session;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Transaction;
import org.ouestware.neo4j.elasticsearch.test.ElasticSearchIntegrationTest;

import java.text.ParseException;
import java.util.Map;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ElasticSearchEventHandlerForCypherIntegrationTest extends ElasticSearchIntegrationTest {

    private final static String label = "Movie";
    private final static String index = "movie";
    private final static String cypher = "MATCH (this) RETURN this.title AS title, this.released AS released , this.tagline AS tagline, size([p=(this)<-[:ACTED_IN]-() | p]) AS nbActor,  {name:'test' } AS actors";

    public ElasticSearchEventHandlerForCypherIntegrationTest() throws ParseException {
        super(index + ":" + label + "(cql`" + cypher + "`cql)");
    }

    @Test
    public void es_indexation_should_work_on() throws Exception {
        Driver driver = getNeo4jDriver();

        // Load some data into Neo4j
        try (Session session = driver.session()) {
            session.run("" +
                    "CREATE (m:Movie { title:'TEST' }) " +
                    "CREATE (p:Person { name:'Ben' }) " +
                    "CREATE (p)-[:ACTED_IN]->(m) " +
                    "");
        }

        // Wait for the elasticsearch refresh
        Thread.sleep(1000);

        // Request to ES
        String query = "{\n" +
                "    \"query\": {\n" +
                "        \"match\" : {\n" +
                "           \"title\": \"TEST\" \n" +
                "        }" +
                "    }\n" +
                "}";

        try (JestClient client = getJestClient()) {
            SearchResult result = client.execute(new Search.Builder(query).build());
            assertTrue(result.getErrorMessage(), result.isSucceeded());
            // testing the nb of result
            assertEquals(1, result.getJsonObject().get("hits").getAsJsonObject().get("total").getAsJsonObject().get("value").getAsInt());

            // retrieve the object
            JsonObject esDoc = result.getJsonObject().get("hits").getAsJsonObject().get("hits").getAsJsonArray().get(0).getAsJsonObject().get("_source").getAsJsonObject();

            // check the object
            assertEquals("TEST", esDoc.get("title").getAsString());
            assertEquals(null, esDoc.get("tagline"));
            assertEquals(null, esDoc.get("release"));
            assertEquals(1, esDoc.get("nbActor").getAsInt());
        }
    }

    @Test
    public void es_indexation_of_a_created_node_should_work() throws Exception {
        // Create a Neo4j node
        Node node = createNode(label, Map.of("title", "t'choupi", "tagline", "t'choupi go to school", "released", "2020"));

        // Request to ES
        try (JestClient client = getJestClient()) {
            JestResult response = client.execute(new Get.Builder(index, getEsNodeId(node)).type("_doc").refresh(true).build());

            // Check the response
            assertEquals(true, response.isSucceeded());
            assertEquals(index, response.getValue("_index"));
            assertEquals(getEsNodeId(node), response.getValue("_id"));

            // Check the response's content
            Map source = response.getSourceAsObject(Map.class);
            assertEquals(asList(label), source.get("@labels"));
            assertEquals(node.getElementId(), source.get("@id"));
            assertEquals(GraphDatabaseSettings.DEFAULT_DATABASE_NAME, source.get("@dbname"));
            assertEquals("t'choupi", source.get("title"));
            assertEquals("t'choupi go to school", source.get("tagline"));
            assertEquals("2020", source.get("released"));
            assertEquals(0.0, source.get("nbActor"));
        }
    }

    @Test
    public void es_desindexation_of_a_deleted_node_should_work() throws Exception {
        // Create a Neo4j node
        Node nodeCreated = createNode(label, Map.of("title", "t'choupi", "tagline", "t'choupi go to school", "released", "2020"));

        // Delete the node
        try (Transaction tx = neo4j.defaultDatabaseService().beginTx()) {
            Node node = tx.getNodeByElementId(nodeCreated.getElementId());
            node.delete();
            tx.commit();
        }

        // Request to ES
        try (JestClient client = getJestClient()) {
            JestResult response = client.execute(new Get.Builder(index, getEsNodeId(nodeCreated)).refresh(true).build());

            // Check the response's content
            assertEquals(false, response.isSucceeded());
        }
    }

    @Test
    public void es_indexation_of_an_updated_property_should_work() throws Exception {
        // Create a Neo4j node
        Node nodeCreated = createNode(label, Map.of("title", "t'choupi", "tagline", "t'choupi go to school", "released", "2020"));

        // Update the node
        try (Transaction tx = neo4j.defaultDatabaseService().beginTx()) {
            Node node = tx.getNodeByElementId(nodeCreated.getElementId());
            node.setProperty("title", "Tchoupi go to the playa");
            tx.commit();
        }

        // Request to ES
        try (JestClient client = getJestClient()) {
            JestResult response = client.execute(new Get.Builder(index, getEsNodeId(nodeCreated)).type("_doc").refresh(true).build());

            // Check the response
            assertEquals(true, response.isSucceeded());
            assertEquals(index, response.getValue("_index"));
            assertEquals(getEsNodeId(nodeCreated), response.getValue("_id"));

            // Check the response's content
            Map source = response.getSourceAsObject(Map.class);
            assertEquals("Tchoupi go to the playa", source.get("title"));
            assertEquals("t'choupi go to school", source.get("tagline"));
            assertEquals("2020", source.get("released"));
            assertEquals(0.0, source.get("nbActor"));
        }
    }

    @Test
    public void es_indexation_of_a_removed_property_should_work() throws Exception {
        // Create a Neo4j node
        Node nodeCreated = createNode(label, Map.of("title", "t'choupi", "tagline", "t'choupi go to school", "released", "2020"));

        // Update the node
        try (Transaction tx = neo4j.defaultDatabaseService().beginTx()) {
            Node node = tx.getNodeByElementId(nodeCreated.getElementId());
            node.removeProperty("tagline");
            tx.commit();
        }

        // Request to ES
        try (JestClient client = getJestClient()) {
            JestResult response = client.execute(new Get.Builder(index, getEsNodeId(nodeCreated)).type("_doc").refresh(true).build());

            // Check the response
            assertEquals(true, response.isSucceeded());
            assertEquals(index, response.getValue("_index"));
            assertEquals(getEsNodeId(nodeCreated), response.getValue("_id"));

            // Check the response's content
            Map source = response.getSourceAsObject(Map.class);
            assertEquals("t'choupi", source.get("title"));
            assertEquals(false, source.containsKey("tagline"));
            assertEquals("2020", source.get("released"));
            assertEquals(0.0, source.get("nbActor"));
        }
    }

    // Can't happened due to the size in the query
    // @Test
    // public void es_indexation_of_a_removed_property_that_is_the_last_indexed_field_should_work() throws Exception {}

    @Test
    public void es_indexation_of_an_added_label_should_work() throws Exception {
        // Create a Neo4j node
        Node nodeCreated = createNode("TEST", Map.of("title", "t'choupi", "tagline", "t'choupi go to school", "released", "2020"));

        // Add label on  the node
        try (Transaction tx = neo4j.defaultDatabaseService().beginTx()) {
            Node node = tx.getNodeByElementId(nodeCreated.getElementId());
            node.addLabel(Label.label(label));
            tx.commit();
        }

        // Request to ES
        try (JestClient client = getJestClient()) {
            JestResult response = client.execute(new Get.Builder(index, getEsNodeId(nodeCreated)).type("_doc").refresh(true).build());

            // Check the response
            assertEquals(true, response.isSucceeded());
            assertEquals(index, response.getValue("_index"));
            assertEquals(getEsNodeId(nodeCreated), response.getValue("_id"));

            // Check the response's content
            Map source = response.getSourceAsObject(Map.class);
            assertEquals("t'choupi", source.get("title"));
            assertEquals("t'choupi go to school", source.get("tagline"));
            assertEquals("2020", source.get("released"));
            assertEquals(0.0, source.get("nbActor"));
        }
    }

    @Test
    public void es_desindexation_of_a_removed_label_should_work() throws Exception {
        // Create a Neo4j node
        Node nodeCreated = createNode(label, Map.of("title", "t'choupi", "tagline", "t'choupi go to school", "released", "2020"));

        // Delete the label
        try (Transaction tx = neo4j.defaultDatabaseService().beginTx()) {
            Node node = tx.getNodeByElementId(nodeCreated.getElementId());
            node.removeLabel(Label.label(label));
            tx.commit();
        }

        // Request to ES
        try (JestClient client = getJestClient()) {
            JestResult response = client.execute(new Get.Builder(index, getEsNodeId(nodeCreated)).refresh(true).build());

            // Check the response's content
            assertEquals(false, response.isSucceeded());
        }
    }

}
