package org.ouestware.neo4j.elasticsearch;

import org.junit.Test;
import org.neo4j.configuration.Config;
import org.ouestware.neo4j.elasticsearch.config.ElasticSearchConfig;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class ElasticSearchXmlConfigParserTest {

    @Test
    public void parsing_xml_config_should_work() throws Exception {
        Config config = this.getConfigForFile("config/test.xml");
        ElasticSearchConfig esConfig = new ElasticSearchConfig(config, "testing");

        assertEquals(3, esConfig.indices.size());
    }

    private Config getConfigForFile(String file) {
        Map<String, String> esConfig = new HashMap();
        Path resourceDirectory = Paths.get("src", "test", "resources");
        String absolutePath = resourceDirectory.toFile().getAbsolutePath();
        esConfig.put("server.directories.plugins", absolutePath);
        esConfig.put("db.elasticsearch.testing.config_file", file);

        Config config = Config.newBuilder(this.getClass().getClassLoader()).setRaw(esConfig).build();

        return config;
    }
}
