package org.ouestware.neo4j.elasticsearch.config;

import java.util.Set;

public class ElasticSearchIndexSpec {

    private final String indexName;
    private Set<String> properties = null;
    private String cypher = null;

    /**
     * Constructor for spec with properties.
     */
    public ElasticSearchIndexSpec(String indexName, Set<String> properties) {
        this.indexName = indexName;
        this.properties = properties;
    }

    /**
     * Constructor for spec with cypher
     */
    public ElasticSearchIndexSpec(String indexName, String cypher) {
        this.indexName = indexName;
        this.cypher = cypher;
    }

    /**
     * Check if this spec is a cypher or prop spec.
     *
     * @return <code>true</code> for cypher spec, <code>false</code> fro props.
     */
    public Boolean isCypherSpec() {
        Boolean result = Boolean.TRUE;
        if (this.cypher == null) {
            result = Boolean.FALSE;
        }
        return result;
    }

    public String getIndexName() {
        return indexName;
    }

    public Set<String> getProperties() {
        return properties;
    }

    public String getCypher() {
        return cypher;
    }

    @Override
    public String toString() {
        String s = getClass().getSimpleName() + " " + indexName + ": (";
        if (properties != null) {
            for (String p : properties) {
                s += p + ",";
            }
        }
        if (cypher != null) {
            s += cypher;
        }
        s += ")";
        return s;
    }
}
