package org.ouestware.neo4j.elasticsearch.config;

import java.text.ParseException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ElasticSearchIndexSpecParser {

    private final static Pattern INDEX_SPEC_RE = Pattern.compile("(?<indexname>[A-Za-z0-9_]+):(?<label>[A-Za-z0-9_]+)\\((?<cypherOrProps>(cql`((?!`cql).)+`cql)|([^\\)]+))\\)");
    private final static Pattern PROPS_SPEC_RE = Pattern.compile("((?!=,)([A-Za-z0-9_]+))+");

    /**
     * Parse the indexation spec and return a map of label / index configuration.
     */
    public static Map<String, List<ElasticSearchIndexSpec>> parseIndexSpec(String spec) throws ParseException {
        if (spec == null || spec.trim().length() == 0) {
            return Collections.emptyMap();
        }

        // Init the result
        Map<String, List<ElasticSearchIndexSpec>> map = new LinkedHashMap<>();

        Matcher matcher = INDEX_SPEC_RE.matcher(spec);
        while (matcher.find()) {
            String indexName = matcher.group("indexname");
            String label = matcher.group("label");
            String cypherOrProps = matcher.group("cypherOrProps");

            // Init the specs list for the label
            List<ElasticSearchIndexSpec> specsForLabel = new ArrayList<ElasticSearchIndexSpec>();
            if (map.containsKey(label)) {
                specsForLabel = map.get(label);
            }

            Boolean newElement = false;

            //  Spec with a cypher query
            if (cypherOrProps.startsWith("cql`") && cypherOrProps.endsWith("`cql")) {
                newElement = true;
                specsForLabel.add(new ElasticSearchIndexSpec(indexName, cypherOrProps.substring(4, cypherOrProps.length() - 4)));
            } else {
                //  Spec with a list of properties
                Matcher propsMatcher = PROPS_SPEC_RE.matcher(cypherOrProps);
                Set<String> props = new HashSet<String>();
                while (propsMatcher.find()) {
                    props.add(propsMatcher.group());
                }
                if (!props.isEmpty()) {
                    newElement = true;
                    specsForLabel.add(new ElasticSearchIndexSpec(indexName, props));
                }
            }

            if (newElement) {
                map.put(label, specsForLabel);
            } else {
                throw new ParseException("Can't parse spec element " + matcher.group(), 0);
            }
        }

        // Spec is defined, but no result so the INDEX_SPEC_RE match nothing
        if (map.size() == 0) {
            throw new ParseException("Can't parse the spec definition, bad format", 0);
        }


        return map;
    }


}
