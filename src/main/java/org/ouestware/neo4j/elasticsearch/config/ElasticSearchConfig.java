package org.ouestware.neo4j.elasticsearch.config;

import org.neo4j.configuration.Config;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.*;

public class ElasticSearchConfig {

    public String dbName;
    public Boolean isEnabled = false;
    public String esUrl;
    public String esUser;
    public String esPassword;
    public Integer esConnTimeout;
    public Integer esReadTimeout;
    public Boolean esDiscovery;
    public Boolean includeId;
    public Boolean includeLabels;
    public Boolean includeDb;
    public Boolean useType;
    public Boolean async;
    public Map<String, List<ElasticSearchIndexSpec>> indices;

    public ElasticSearchConfig(Config config, String dbName) throws Exception {
        this.isEnabled = false;
        ElasticSearchNeo4jConfig esConfig = config.getGroups(ElasticSearchNeo4jConfig.class).get(dbName);
        if (esConfig != null) {

            this.dbName = dbName;
            this.esUrl = config.get(esConfig.CONFIG_ES_URL);
            this.esDiscovery = config.get(esConfig.CONFIG_ES_DISCOVERY);
            this.esUser = config.get(esConfig.CONFIG_ES_USER);
            this.esPassword = config.get(esConfig.CONFIG_ES_PASSWORD);
            this.esConnTimeout = config.get(esConfig.CONFIG_ES_CONN_TIMEOUT);
            this.esReadTimeout = config.get(esConfig.CONFIG_ES_READ_TIMEOUT);
            this.useType = config.get(esConfig.CONFIG_ES_USE_INDEX_TYPE);
            this.includeId = config.get(esConfig.CONFIG_ES_INCLUDE_ID);
            this.includeLabels = config.get(esConfig.CONFIG_ES_INCLUDE_LABELS);
            this.includeDb = config.get(esConfig.CONFIG_ES_INCLUDE_DB);
            this.async = config.get(esConfig.CONFIG_ES_ASYNC);

            if (config.get(esConfig.CONFIG_ES_INDEX_SPEC) != null) {
                this.indices = ElasticSearchIndexSpecParser.parseIndexSpec(config.get(esConfig.CONFIG_ES_INDEX_SPEC));
            }
            if (config.get(esConfig.CONFIG_FILE_PATH) != null) {
                String filePath = config.get(config.getSetting("server.directories.plugins")) + "/" + config.get(esConfig.CONFIG_FILE_PATH);
                this.indices = new LinkedHashMap<>();
                this.parseXmlConfig(filePath, esConfig);
            }
            this.isEnabled = true;
        }
    }

    private void parseXmlConfig(String filePath, ElasticSearchNeo4jConfig esConfig) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setIgnoringComments(true);
        dbf.setIgnoringElementContentWhitespace(true);
        DocumentBuilder db = dbf.newDocumentBuilder();

        File file = new File(filePath);
        Document doc = db.parse(file);
        Element root = doc.getDocumentElement();

        NodeList children = root.getChildNodes();
        for (int itr = 0; itr < children.getLength(); itr++) {
            Node node = children.item(itr);

            if (node.getNodeName().equals(esConfig.CONFIG_ES_URL.name())) {
                this.esUrl = node.getTextContent();
            }
            if (node.getNodeName().equals(esConfig.CONFIG_ES_USER.name())) {
                this.esUrl = node.getTextContent();
            }
            if (node.getNodeName().equals(esConfig.CONFIG_ES_PASSWORD.name())) {
                this.esPassword = node.getTextContent();
            }
            if (node.getNodeName().equals(esConfig.CONFIG_ES_DISCOVERY.name())) {
                this.esDiscovery = node.getTextContent().toLowerCase().equals("true") ? true : false;
            }
            if (node.getNodeName().equals(esConfig.CONFIG_ES_CONN_TIMEOUT.name())) {
                this.esConnTimeout = Integer.parseInt(node.getTextContent());
            }
            if (node.getNodeName().equals(esConfig.CONFIG_ES_READ_TIMEOUT.name())) {
                this.esReadTimeout = Integer.parseInt(node.getTextContent());
            }
            if (node.getNodeName().equals(esConfig.CONFIG_ES_USE_INDEX_TYPE.name())) {
                this.useType = node.getTextContent().toLowerCase().equals("true") ? true : false;
            }
            if (node.getNodeName().equals(esConfig.CONFIG_ES_INCLUDE_ID.name())) {
                this.includeId = node.getTextContent().toLowerCase().equals("true") ? true : false;
            }
            if (node.getNodeName().equals(esConfig.CONFIG_ES_INCLUDE_LABELS.name())) {
                this.includeLabels = node.getTextContent().toLowerCase().equals("true") ? true : false;
            }
            if (node.getNodeName().equals(esConfig.CONFIG_ES_INCLUDE_DB.name())) {
                this.includeDb = node.getTextContent().toLowerCase().equals("true") ? true : false;
            }
            if (node.getNodeName().equals(esConfig.CONFIG_ES_ASYNC.name())) {
                this.async = node.getTextContent().toLowerCase().equals("true") ? true : false;
            }

            if (node.getNodeName().equals("indices")) {
                NodeList indices = node.getChildNodes();
                for (int i = 0; i < indices.getLength(); i++) {
                    Node index = indices.item(i);
                    if (index.getNodeName().equals("index")) {
                        String indexName = null;
                        String label = null;
                        String cypher = null;
                        Set<String> properties = new HashSet<>();

                        NodeList configList = index.getChildNodes();
                        for (int j = 0; j < configList.getLength(); j++) {
                            Node config = configList.item(j);

                            if (config.getNodeName().equals("name")) {
                                indexName = config.getTextContent();
                            }

                            if (config.getNodeName().equals("label")) {
                                label = config.getTextContent();
                            }

                            if (config.getNodeName().equals("cypher")) {
                                cypher = config.getTextContent();
                            }
                            if (config.getNodeName().equals("properties")) {
                                NodeList props = config.getChildNodes();
                                for (int k = 0; k < props.getLength(); k++) {
                                    Node prop = props.item(k);
                                    if (prop.getNodeName().equals("property")) {
                                        properties.add(prop.getTextContent());
                                    }
                                }
                            }
                        }

                        if (label != null && indexName != null) {
                            ElasticSearchIndexSpec spec = null;
                            if (!properties.isEmpty()) {
                                spec = new ElasticSearchIndexSpec(indexName, properties);
                            }
                            if (cypher != null) {
                                spec = new ElasticSearchIndexSpec(indexName, cypher);
                            }
                            if (spec != null) {
                                List<ElasticSearchIndexSpec> labelConfigs = new ArrayList<>();
                                if (this.indices.containsKey(label)) {
                                    labelConfigs = this.indices.get(label);
                                }
                                labelConfigs.add(spec);
                                this.indices.put(label, labelConfigs);
                            }
                        }
                    }
                }
            }
        }
    }
}
