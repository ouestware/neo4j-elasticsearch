package org.ouestware.neo4j.elasticsearch.config;

import org.neo4j.annotations.service.ServiceProvider;
import org.neo4j.configuration.*;
import org.neo4j.graphdb.config.Setting;

import static org.neo4j.configuration.SettingValueParsers.*;

@ServiceProvider
public class ElasticSearchNeo4jConfig implements GroupSetting {

    // Prefix for the configuration keys
    public final String PREFIX = "db.elasticsearch";
    public String name;

    @Description("Path to the XML configuration file")
    @DocumentedDefaultValue("elasticsearch.xml")
    public final Setting<String> CONFIG_FILE_PATH;

    @Description("ElasticSearch URL")
    @DocumentedDefaultValue("http://localhost:9200")
    public final Setting<String> CONFIG_ES_URL;

    @Description("ElasticSearch index specification")
    public final Setting<String> CONFIG_ES_INDEX_SPEC;

    @Description("Discover other ElasticSearch cluster node ?")
    @DocumentedDefaultValue("false")
    public final Setting<Boolean> CONFIG_ES_DISCOVERY;

    @Description("Should ElasticSearch indexation includes node's ID ?")
    @DocumentedDefaultValue("true")
    public final Setting<Boolean> CONFIG_ES_INCLUDE_ID;

    @Description("Should ElasticSearch indexation includes node's Labels ?")
    @DocumentedDefaultValue("true")
    public final Setting<Boolean> CONFIG_ES_INCLUDE_LABELS;

    @Description("Should ElasticSearch indexation includes database name ?")
    @DocumentedDefaultValue("true")
    public final Setting<Boolean> CONFIG_ES_INCLUDE_DB;

    @Description("Should ElasticSearch indexation use labels as types ?")
    @DocumentedDefaultValue("false")
    public final Setting<Boolean> CONFIG_ES_USE_INDEX_TYPE;

    @Description("Should ElasticSearch indexation use async ?")
    @DocumentedDefaultValue("true")
    public final Setting<Boolean> CONFIG_ES_ASYNC;

    @Description("ElasticSearch user's name")
    public final Setting<String> CONFIG_ES_USER ;

    @Description("ElasticSearch user's password")
    public final Setting<String> CONFIG_ES_PASSWORD;

    @Description("ElasticSearch connection timeout (ms)")
    public final Setting<Integer> CONFIG_ES_CONN_TIMEOUT;

    @Description("ElasticSearch read timeout in (ms)")
    public final Setting<Integer> CONFIG_ES_READ_TIMEOUT;

    private ElasticSearchNeo4jConfig(String dbname) {
        if (dbname == null) {
            throw new IllegalArgumentException("ElasticSearchConfig can not be created for scope: " + dbname);
        }
        this.name = dbname;
        this.CONFIG_ES_URL = getBuilder("host_name", STRING, "http://localhost:9200").build();
        this.CONFIG_ES_INDEX_SPEC = getBuilder("index_spec", STRING, "").build();
        this.CONFIG_ES_DISCOVERY = getBuilder("discovery", BOOL, Boolean.FALSE).build();
        this.CONFIG_ES_INCLUDE_ID = getBuilder("include_id_field", BOOL, Boolean.TRUE).build();
        this.CONFIG_ES_INCLUDE_LABELS = getBuilder("include_labels_field", BOOL, Boolean.TRUE).build();
        this.CONFIG_ES_INCLUDE_DB = getBuilder("include_db_field", BOOL, Boolean.FALSE).build();
        this.CONFIG_ES_USE_INDEX_TYPE = getBuilder("type_mapping", BOOL, Boolean.FALSE).build();
        this.CONFIG_ES_ASYNC = getBuilder("async", BOOL, Boolean.TRUE).build();
        this.CONFIG_ES_USER = getBuilder("user", STRING, null).build();
        this.CONFIG_ES_PASSWORD = getBuilder("password", STRING, null).build();
        this.CONFIG_ES_CONN_TIMEOUT = getBuilder("connection_timeout", INT, null).build();
        this.CONFIG_ES_READ_TIMEOUT = getBuilder("read_timeout", INT, null).build();
        this.CONFIG_FILE_PATH = getBuilder("config_file", STRING, null).build();
    }

    //For serviceloading
    public ElasticSearchNeo4jConfig() {
        this("testing");
    }

    public static ElasticSearchNeo4jConfig forDatabase(String dbname) {
        return new ElasticSearchNeo4jConfig(dbname);
    }

    @Override
    public String name() {
        return this.name;
    }

    @Override
    public String getPrefix() {
        return PREFIX;
    }

    private <T> SettingBuilder<T> getBuilder(String suffix, SettingValueParser<T> parser, T defaultValue) {
        return GroupSettingHelper.getBuilder(getPrefix(), name(), suffix, parser, defaultValue);
    }

}
