package org.ouestware.neo4j.elasticsearch;

import org.neo4j.configuration.Config;
import org.neo4j.dbms.api.DatabaseManagementService;
import org.neo4j.kernel.internal.GraphDatabaseAPI;
import org.neo4j.kernel.lifecycle.LifecycleAdapter;
import org.neo4j.logging.Log;
import org.neo4j.logging.internal.LogService;
import org.neo4j.procedure.impl.GlobalProceduresRegistry;
import org.ouestware.neo4j.elasticsearch.config.ElasticSearchConfig;
import org.ouestware.neo4j.elasticsearch.config.ElasticSearchIndexSpec;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.neo4j.configuration.GraphDatabaseSettings.SYSTEM_DATABASE_NAME;

/**
 * @author mh
 * @since 25.04.15
 */
public class ElasticSearchLifecycle extends LifecycleAdapter {

    private final LogService log;
    private final Config config;
    private final GraphDatabaseAPI db;
    private final DatabaseManagementService dbms;
    private final GlobalProceduresRegistry globalProceduresRegistry;

    // logger
    private final Log logger;
    // The index Specification
    private Map<String, List<ElasticSearchIndexSpec>> indices = new HashMap<>();
    // The ES client to index nodes
    private ElasticSearchClient client;
    // The ES Event Handler
    private ElasticSearchEventListener listener;

    /**
     * Create the ElasticSearch Extension.
     *
     * @param log                      Neo4j log service
     * @param config                   Neo4j configuration
     * @param db                       Neo4j graph database API
     * @param dbms                     DB Management service
     * @param globalProceduresRegistry procedurre registery
     */
    public ElasticSearchLifecycle(LogService log, Config config, DatabaseManagementService dbms, GraphDatabaseAPI db, GlobalProceduresRegistry globalProceduresRegistry) {
        logger = log.getUserLog(ElasticSearchLifecycle.class);
        logger.info(String.format("[%s] Creating ElasticSearchLifecycle", db.databaseName()));

        // save params
        this.config = config;
        this.log = log;
        this.db = db;
        this.dbms = dbms;
        this.globalProceduresRegistry = globalProceduresRegistry;

        // expose this  instance via `@Context ElasticSearchLifecycle config`
        globalProceduresRegistry.registerComponent((Class<ElasticSearchLifecycle>) getClass(), ctx -> this, true);

    }

    @Override
    public void init() throws Exception {
        if (!SYSTEM_DATABASE_NAME.equals(db.databaseName())) {
            logger.info(String.format("[%s] Starting ElasticSearchLifecycle", db.databaseName()));

            try {
                ElasticSearchConfig esConfig = new ElasticSearchConfig(config, db.databaseName());
                if (esConfig.isEnabled) {
                    // if `type_mapping = true` with a label that start with a `_` => make a clear message in logs
                    if (esConfig.useType && esConfig.indices.keySet().stream().anyMatch(s -> s.startsWith("_"))) {
                        logger.warn("Elastic integration - ES types can't start with a `_` and one of your indexed labels match this condition. You should rename your label or disable the type mapping.");
                    }
                    if (esConfig.indices.size() == 0) {
                        logger.warn("Elastic integration - Configuration is missing or empty");
                    }

                    // Create the ES client
                    client = new ElasticSearchClient(esConfig);

                    // Create the Event Listener
                    listener = new ElasticSearchEventListener(client);

                    // Register Event Listener
                    dbms.registerTransactionEventListener(db.databaseName(), listener);
                } else {
                    logger.info(String.format("ElasticSearch configuration is not defined for database %s", db.databaseName()));
                }
            } catch (Exception e) {
                logger.error("Elastic integration - Bad configuration.");
            }
        }
    }


    @Override
    public void shutdown() {
        if (!SYSTEM_DATABASE_NAME.equals(db.databaseName())) {
            logger.info(String.format("[%s] Stopping ElasticSearch lifecycle", db.databaseName()));

            // Remove the event handler
            if (listener != null) {
                dbms.unregisterTransactionEventListener(db.databaseName(), listener);
            }

            // Shutdown the ES client
            if (client != null) {
                client.shutdown();
            }
        }
    }

}
